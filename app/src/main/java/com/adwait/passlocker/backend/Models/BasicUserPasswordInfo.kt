package com.adwait.passlocker.backend.Models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Adwait on 4/1/2017.
 */
open class BasicUserPasswordInfo : RealmObject(), IBasicInfo {
    override lateinit var host: String
    @PrimaryKey
    var id = UUID.randomUUID().toString()
    override lateinit var name: String
    var password: String? = null
    var addedAttributes: RealmList<AddtionalAttribute>? = null

}