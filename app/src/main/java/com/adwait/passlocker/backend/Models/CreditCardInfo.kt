package com.adwait.passlocker.backend.Models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Adwait on 4/1/2017.
 */
open class CreditCardInfo : RealmObject(), IBasicInfo {
    override lateinit var host: String
    override lateinit var name: String
    var cardNo: String? = null
    var toDate: Date? = null
    var fromDate: Date? = null
    var pin: String? = null
    @PrimaryKey
    var id = UUID.randomUUID().toString()
    var cvv: String? = null
    var addedAttributes: RealmList<AddtionalAttribute>? = null

}