package com.adwait.passlocker.backend.Models

/**
 * Created by adwait on 6/7/17.
 */
interface IBasicInfo {
    val name: String?
    val host: String
}