package com.adwait.passlocker.backend.Models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Adwait on 4/1/2017.
 */
open class NetBankingInfo : RealmObject(), IBasicInfo {
    private var iPin: String? = null
    var transactionPassword: String? = null
    var accountNo: String? = null
    var iFSC: String? = null
    override lateinit var host: String
    override lateinit var name: String
    var username: String? = null
    var password: String? = null
    @PrimaryKey
    var id = UUID.randomUUID().toString()
    var addedAttributes: RealmList<AddtionalAttribute>? = null

    fun getiPin(): String? {
        return iPin
    }

    fun setiPin(iPin: String?) {
        this.iPin = iPin
    }

}