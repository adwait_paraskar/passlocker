package com.adwait.passlocker.backend

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by Adwait on 5/6/2017.
 */
object RealmManager {
    private var sRealmObject: Realm? = null
    fun defaultInstance(context: Context?): Realm? {
        if (sRealmObject == null && context != null) {
            val key = Keychain(context).getKey() ?: return null
            val configuration = RealmConfiguration.Builder().encryptionKey(key).build()
            sRealmObject = Realm.getInstance(configuration)
        }
        return sRealmObject
    }
}