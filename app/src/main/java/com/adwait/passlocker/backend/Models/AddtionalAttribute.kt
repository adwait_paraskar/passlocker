package com.adwait.passlocker.backend.Models

import io.realm.RealmObject

/**
 * Created by Adwait on 4/23/2017.
 */
open class AddtionalAttribute : RealmObject() {
    var key: String? = null
    var value: String? = null

}