package com.adwait.passlocker.backend

import android.content.Context
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import com.adwait.passlocker.R
import java.security.KeyStore
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

class Keychain (var context: Context) {

    fun getKey(): ByteArray? {
        val generatedKeystore = getKeyFromKeyStore()
        generatedKeystore?.let {
            val key = fetchDecryptedKeyFromSharedPref(it)
            if (key == null) {
                val generatedKey = generateKeyForStorage()
                if (generatedKey != null) {
                    encryptAndSaveKeyInStorage(generatedKey, it)
                    return generatedKey
                }
            } else {
                return key
            }
        }
        return null
    }

     private fun getKeyFromKeyStore(): SecretKey? {
        var keystore = KeyStore.getInstance(ANDROID_KEYSTORE)
         keystore.load(null)
         if(!keystore.containsAlias(KEY_ALIAS)) {
             val generator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES,
                 ANDROID_KEYSTORE)
             val spec = KeyGenParameterSpec.Builder(KEY_ALIAS, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                 .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                 .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                 .setRandomizedEncryptionRequired(false)
                 .build()
             generator.init(spec)
             return generator.generateKey()
         }
         return (keystore.getEntry(KEY_ALIAS,null) as? KeyStore.SecretKeyEntry)?.secretKey
    }

    private fun generateKeyForStorage(): ByteArray? {
//        val generator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES);
//        generator.init(256)
//        return generator.generateKey().encoded;
        val key = ByteArray(64)
        SecureRandom().nextBytes(key)
        return key
    }

    private fun encryptAndSaveKeyInStorage(key: ByteArray, secretKey: SecretKey) {
        val preferences = this.context.getSharedPreferences(this.context.getString(R.string.preferences), Context.MODE_PRIVATE)
        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        val encryptedKey = encrypt(cipher,key)
        val editor = preferences.edit()
        val encodedKey = Base64.encodeToString(encryptedKey,Base64.DEFAULT)
        val iv = Base64.encodeToString(cipher.iv,Base64.DEFAULT)
        editor.putString(KEY_PREF, encodedKey)
        editor.putString(KEY_IV, iv)
        editor.commit()
    }

    private fun fetchDecryptedKeyFromSharedPref(secretKey: SecretKey): ByteArray? {
        val preferences = this.context.getSharedPreferences(this.context.getString(R.string.preferences), Context.MODE_PRIVATE)
        val storedKeyInString = preferences.getString(KEY_PREF, "")
        val storedIvInString = preferences.getString(KEY_IV, "")
        if (storedKeyInString != null && storedKeyInString.isNotEmpty() && storedIvInString != null && storedIvInString.isNotEmpty()) {
            val encryptedKey = Base64.decode(storedKeyInString, Base64.DEFAULT)
            val iv = Base64.decode(storedIvInString, Base64.DEFAULT)
            val ivParams = IvParameterSpec(iv)
            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParams)
            return decrypt(cipher,encryptedKey)
        }
        return null
    }

    private fun decrypt(cipher: Cipher, data: ByteArray): ByteArray? {
        return cipher.doFinal(data)
    }

    private fun encrypt(cipher: Cipher, data: ByteArray): ByteArray? {
        return cipher.doFinal(data)
    }

    companion object {
        val KEY_ALIAS = "PassLocker";
        val ANDROID_KEYSTORE = "AndroidKeyStore"
        val KEY_PREF = "KeyStoredInPreferences"
        val KEY_IV = "IV"
    }
}