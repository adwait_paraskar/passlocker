package com.adwait.passlocker.application

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import java.security.SecureRandom

/**
 * Created by Adwait on 4/15/2017.
 */
class PassLockerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initRealm()
    }

    private fun initRealm() {
        Realm.init(this)
        val key = ByteArray(64)
        SecureRandom().nextBytes(key)
        val realmConfiguration = RealmConfiguration.Builder().encryptionKey(key).build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }
}