package com.adwait.passlocker.ui.calculator

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.DataBindingUtil
import com.adwait.passlocker.backend.Models.Pin
import com.adwait.passlocker.backend.RealmManager
import com.adwait.passlocker.R
import com.adwait.passlocker.databinding.ActivityMainBinding
import com.adwait.passlocker.ui.locker.LockerActivity

/**
 * Created by Adwait on 3/11/2017.
 */
class CalculatorActivity : Activity(), INumberPadListener {
    private var binding: ActivityMainBinding? = null
    private var operand1 = Double.NaN
    private var operand2 = Double.NaN
    private var currentOperator: INumberPadListener.Operator? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding?.numberPad?.setNumberPadListener(this)
    }

    //    private void loadDummy(){
//        BasicUserPasswordInfo info = new BasicUserPasswordInfo();
//        info.setName("User1");
//        info.setHost("FaceBook");
//        info.setPassword("password");
//
//        NetBankingInfo netBankingInfo = new NetBankingInfo();
//        netBankingInfo.setName("HDFC");
//        netBankingInfo.setHost("HDFC");
//        netBankingInfo.setAccountNo("1234");
//        RealmManager.defaultInstance().beginTransaction();
//        RealmManager.defaultInstance().copyToRealm(info);
//        RealmManager.defaultInstance().copyToRealm(netBankingInfo);
//        RealmManager.defaultInstance().commitTransaction();
//    }
    private fun actionPerformed(operator: INumberPadListener.Operator) {
        if (!java.lang.Double.isNaN(operand1)) {
            try {
                operand2 = binding!!.editor.text.toString().toDouble()
            } catch (exception: NumberFormatException) {
                if (operator !== INumberPadListener.Operator.EQUALS) {
                    val string = binding!!.textView.text.toString()
                    if (currentOperator === INumberPadListener.Operator.NONE) {
                        binding!!.textView.text = string + operator
                    } else {
                        binding!!.textView.text = string.substring(0, string.length - 1) + operator.toString()
                    }
                    currentOperator = operator
                } else {
                    currentOperator = INumberPadListener.Operator.NONE
                    val string = binding!!.textView.text.toString()
                    binding!!.textView.text = string.substring(0, string.length - 1)
                }
                return
            }
            operand1 = performOperation(operand1, operand2, currentOperator)
            if (operator === INumberPadListener.Operator.EQUALS) {
                binding!!.textView.text = operand1.toString() + ""
                operand1 = Double.NaN
            } else {
                binding!!.textView.text = operand1.toString() + operator.toString()
                currentOperator = operator
            }
            binding!!.editor.text = ""
        } else {
            operand1 = try {
                binding!!.textView.text.toString().toDouble()
            } catch (exception: NumberFormatException) {
                try {
                    binding!!.editor.text.toString().toDouble()
                } catch (numberException: NumberFormatException) {
                    0.0
                }
            }
            if (operator !== INumberPadListener.Operator.EQUALS) {
                binding!!.textView.text = operand1.toString() + operator.toString()
                currentOperator = operator
            } else {
                binding!!.textView.text = operand1.toString() + ""
                currentOperator = INumberPadListener.Operator.NONE
            }
            binding!!.editor.text = ""
        }
    }

    private fun performOperation(operand1: Double, operand2: Double, operator: INumberPadListener.Operator?): Double {
        return when (operator) {
            INumberPadListener.Operator.ADD -> operand1 + operand2
            INumberPadListener.Operator.SUBTRACT -> operand1 - operand2
            INumberPadListener.Operator.MULTIPLY -> operand1 * operand2
            INumberPadListener.Operator.DIVIDE -> operand1 / operand2
            INumberPadListener.Operator.EQUALS -> operand1
            else -> 0.0
        }
    }

    override fun numberClicked(number: Int) {
        binding!!.editor.text = binding!!.editor.text.toString() + number
    }

    override fun operatorClicked(operator: INumberPadListener.Operator) {
        when (operator) {
            INumberPadListener.Operator.DELETE -> {
                val string = binding!!.editor.text.toString()
                if (!string.isEmpty()) {
                    binding!!.editor.text = string.substring(0, string.length - 1)
                }
            }
            INumberPadListener.Operator.AC -> {
                binding!!.editor.text = ""
                binding!!.textView.text = ""
                operand1 = Double.NaN
                operand2 = Double.NaN
            }
            INumberPadListener.Operator.DOT -> {
                binding!!.editor.text = binding!!.editor.text.toString() + "."
                //Log.e("Password", binding!!.editor.text.toString())
                //Log.e("Pin", RealmManager.defaultInstance()?.where(Pin::class.java).findFirst().pin)
                if (binding!!.editor.text.toString().equals(RealmManager.defaultInstance(applicationContext)?.where(Pin::class.java)?.findFirst()?.pin, ignoreCase = true)) { //Open list screen
                    val intent = Intent(this@CalculatorActivity, LockerActivity::class.java)
                    startActivity(intent)
                    finish()
                    Toast.makeText(this@CalculatorActivity, "Password correct", Toast.LENGTH_LONG).show()
                }
            }
            else -> actionPerformed(operator)
        }
    }
}