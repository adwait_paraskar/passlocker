package com.adwait.passlocker.ui.customViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.adwait.passlocker.R
import com.adwait.passlocker.ui.calculator.INumberPadListener
import com.adwait.passlocker.ui.calculator.NumberPad

/**
 * Created by Adwait on 8/15/2017.
 */
class PinFragment : Fragment() {
    private var pinAttempt = String()
    private var mPin = String()
    private var textView: TextView? = null
    private var pinView1: ImageView? = null
    private var pinView2: ImageView? = null
    private var pinView3: ImageView? = null
    private var pinView4: ImageView? = null
    private var numberPad: NumberPad? = null
    private var mPinsetListener: IPinSetListener? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_change_pin, container, false)
        textView = view.findViewById<View>(R.id.textView) as TextView
        pinView1 = view.findViewById<View>(R.id.pin_view_1) as ImageView
        pinView2 = view.findViewById<View>(R.id.pin_view_2) as ImageView
        pinView3 = view.findViewById<View>(R.id.pin_view_3) as ImageView
        pinView4 = view.findViewById<View>(R.id.pin_view_4) as ImageView
        numberPad = view.findViewById<View>(R.id.number_pad) as NumberPad
        numberPad?.setNumberPadListener(numberPadListener)
        return view
    }

    private val numberPadListener: INumberPadListener = object : INumberPadListener {
        override fun numberClicked(number: Int) {
            handleButtonClick(number)
        }

        override fun operatorClicked(operator: INumberPadListener.Operator) {}
    }

    private fun handleButtonClick(code: Int) {
        when (pinAttempt.length) {
            0 -> {
                pinView1?.setImageResource(R.drawable.filledoval)
                pinAttempt += code
            }
            1 -> {
                pinView2?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.filledoval))
                pinAttempt += code
            }
            2 -> {
                pinView3?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.filledoval))
                pinAttempt += code
            }
            3 -> {
                pinView4?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.filledoval))
                pinAttempt += code
                if (mPin.length == 0) {
                    mPin = pinAttempt
                    pinAttempt = ""
                    textView?.text = "Confirm Pin"
                    clearPinView()
                } else if (mPin.equals(pinAttempt, ignoreCase = true)) {
                    savePin()
                } else {
                    textView?.text = "Pins Do not match. Try again"
                    pinAttempt = ""
                    mPin = ""
                    clearPinView()
                }
            }
        }
    }

    private fun savePin() {
        mPinsetListener?.pinSet("$mPin..")
    }

    private fun clearPinView() {
        pinView1?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.ovalshape))
        pinView2?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.ovalshape))
        pinView3?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.ovalshape))
        pinView4?.setImageDrawable(requireContext().resources.getDrawable(R.drawable.ovalshape))
    }

    companion object {
        fun getInstance(listener: IPinSetListener?): PinFragment {
            val fragment = PinFragment()
            fragment.mPinsetListener = listener
            return fragment
        }
    }
}