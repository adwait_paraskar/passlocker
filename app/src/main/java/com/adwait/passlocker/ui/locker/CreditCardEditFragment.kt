package com.adwait.passlocker.ui.locker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.adwait.passlocker.backend.Models.CreditCardInfo
import com.adwait.passlocker.backend.RealmManager
import com.adwait.passlocker.R
import com.adwait.passlocker.ui.customViews.CreditCardEditText
import com.adwait.passlocker.ui.customViews.DatePickerDialog
import com.adwait.passlocker.ui.customViews.DatePickerDialog.IDateChangedListener
import com.adwait.passlocker.ui.customViews.PickerDataSource
import java.util.*

/**
 * Created by Adwait on 6/12/2017.
 */
class CreditCardEditFragment : Fragment(), IDateChangedListener {
    var mToField: EditText? = null
    var mFromField: EditText? = null
    var mCreditCardInfo: CreditCardInfo? = null
    var mCvvField: EditText? = null
    var mPinField: EditText? = null
    var mNameField: EditText? = null
    var mHostNameField: EditText? = null
    var mCardField: CreditCardEditText? = null
    var mSaveButton: Button? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_credit_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        if (mCreditCardInfo != null) {
            populateFields()
        }
    }

    override fun onResume() {
        super.onResume()
        if (mCreditCardInfo != null) {
            (activity as LockerActivity?)?.setTitle(mCreditCardInfo?.name)
        } else {
            (activity as LockerActivity?)!!.setTitle("Add New Card")
        }
    }

    private fun initViews(view: View) {
        mToField = view.findViewById<View>(R.id.toField) as EditText
        mFromField = view.findViewById<View>(R.id.fromField) as EditText
        mCardField = view.findViewById<View>(R.id.cardNoField) as CreditCardEditText
        mCvvField = view.findViewById<View>(R.id.cvvField) as EditText
        mPinField = view.findViewById<View>(R.id.pinField) as EditText
        mNameField = view.findViewById<View>(R.id.cardNameField) as EditText
        mHostNameField = view.findViewById<View>(R.id.hostField) as EditText
        mSaveButton = view.findViewById<View>(R.id.saveButton) as Button
        mSaveButton!!.setOnClickListener { saveInfo() }
        mToField?.setOnClickListener { launchPicker() }
        mToField?.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                launchPicker()
            }
        }
        mFromField?.setOnClickListener { launchPicker() }
        mFromField?.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                launchPicker()
            }
        }
    }

    private fun populateFields() {
        mNameField?.setText(mCreditCardInfo?.name ?: "")
        mHostNameField?.setText(mCreditCardInfo?.host ?: "")
        mCardField?.setText(mCreditCardInfo?.cardNo ?: "")
        mPinField?.setText(mCreditCardInfo?.pin ?: "")
        mCvvField?.setText(mCreditCardInfo?.cvv  ?: "")
        val calendar = Calendar.getInstance()
        if (mCreditCardInfo?.fromDate != null) {
            calendar.time = mCreditCardInfo?.fromDate
            // Add 1 to month to convert to readable form
            mFromField?.setText("${calendar[Calendar.MONTH] + 1} / ${calendar[Calendar.YEAR]}")
        }
        if (mCreditCardInfo?.toDate != null) {
            calendar.time = mCreditCardInfo?.toDate
            mToField?.setText("${calendar[Calendar.MONTH] + 1} / ${calendar[Calendar.YEAR]}")
        }
    }

    private fun saveInfo() {
        if (mCreditCardInfo == null) {
            mCreditCardInfo = CreditCardInfo()
        }
        RealmManager.defaultInstance(null)?.beginTransaction()
        if (mNameField?.text != null) mCreditCardInfo?.name = mNameField?.text.toString()
        if (mHostNameField?.text != null) mCreditCardInfo?.host = mHostNameField?.text.toString()
        if (mCardField?.cardNo != null) mCreditCardInfo?.cardNo = mCardField?.cardNo.toString()
        mCreditCardInfo?.pin = mPinField?.text.toString()
        if (mCvvField?.text != null) mCreditCardInfo?.cvv = mCvvField?.text.toString()
        if (mToField!!.text != null) {
            val dateString = mToField!!.text.toString().replace(" ", "").split("/").toTypedArray()
            val date = Date()
            val calendar = Calendar.getInstance()
            calendar.time = date
            val month = dateString[0].toInt() - 1
            calendar[Calendar.MONTH] = month
            calendar[Calendar.YEAR] = dateString[1].toInt()
            mCreditCardInfo?.toDate = calendar.time
        }
        if (!mFromField!!.text.toString().isEmpty()) {
            val dateString = mFromField!!.text.toString().replace(" ", "").split("/").toTypedArray()
            val date = Date()
            val calendar = Calendar.getInstance()
            calendar.time = date
            //Since calendar's month starts with 0 not 1.
            val month = dateString[0].toInt() - 1
            calendar[Calendar.MONTH] = month
            calendar[Calendar.YEAR] = dateString[1].toInt()
            mCreditCardInfo?.fromDate = calendar.time
        }
        RealmManager.defaultInstance(null)?.copyToRealmOrUpdate<CreditCardInfo>(mCreditCardInfo)
        RealmManager.defaultInstance(null)?.commitTransaction()
        requireActivity().supportFragmentManager.popBackStackImmediate()
    }

    private fun launchPicker() {
        val dataSource = PickerDataSource()
        val date = Date()
        val calendar = Calendar.getInstance()
        calendar.time = date
        dataSource.currentMonth = calendar[Calendar.MONTH] + 1
        dataSource.currentYear = calendar[Calendar.YEAR]
        val dialog = DatePickerDialog(requireContext(), 0, this, dataSource)
        dialog.show()
    }

    override fun dateSet(month: Int?, year: Int?) {
        if (mToField?.isFocused == true) {
            mToField?.setText("$month / $year")
        } else if (mFromField!!.isFocused) {
            mFromField?.setText("$month / $year")
        }
    }

    companion object {
        fun newInstance(creditCardInfo: CreditCardInfo?): CreditCardEditFragment {
            val fragment = CreditCardEditFragment()
            fragment.mCreditCardInfo = creditCardInfo
            return fragment
        }
    }
}