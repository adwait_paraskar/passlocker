package com.adwait.passlocker.ui.customViews

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.widget.NumberPicker
import androidx.annotation.StyleRes
import androidx.appcompat.app.AlertDialog
import com.adwait.passlocker.R

/**
 * Created by Adwait on 6/18/2017.
 */
class DatePickerDialog(context: Context, @StyleRes themeResId: Int, private val mDateChangedListener: IDateChangedListener, private val mDataSource: PickerDataSource) : AlertDialog(context, themeResId), DialogInterface.OnClickListener {
    interface IDateChangedListener {
        fun dateSet(month: Int?, year: Int?)
    }

    private var monthPicker: NumberPicker? = null
    private var yearPicker: NumberPicker? = null
    private fun initView(pickerView: View) {
        monthPicker = pickerView.findViewById<View>(R.id.monthPicker) as NumberPicker
        yearPicker = pickerView.findViewById<View>(R.id.yearPicker) as NumberPicker
        monthPicker?.minValue = mDataSource.startMonth
        monthPicker?.maxValue = mDataSource.endMonth
        yearPicker?.minValue = mDataSource.starYear
        yearPicker?.maxValue = mDataSource.endYear
        monthPicker?.value = mDataSource.currentMonth
        yearPicker?.value = mDataSource.currentYear
    }

    override fun onClick(dialog: DialogInterface, which: Int) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            mDateChangedListener.dateSet(monthPicker?.value, yearPicker?.value)
        }
    }

    init {
        val pickerView = LayoutInflater.from(context).inflate(R.layout.date_picker, null)
        setView(pickerView)
        initView(pickerView)
        setButton(DialogInterface.BUTTON_POSITIVE, "Choose", this)
        setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", this)
    }
}