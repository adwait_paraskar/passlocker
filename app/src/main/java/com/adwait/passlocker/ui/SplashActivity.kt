package com.adwait.passlocker.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.adwait.passlocker.backend.Models.Pin
import com.adwait.passlocker.backend.RealmManager
import com.adwait.passlocker.R
import com.adwait.passlocker.ui.calculator.CalculatorActivity
import com.adwait.passlocker.ui.customViews.IPinSetListener
import com.adwait.passlocker.ui.customViews.PinFragment
import com.adwait.passlocker.ui.locker.LockerActivity

/**
 * Created by Adwait on 4/15/2017.
 */
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val preferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        val pinSaved = preferences.getBoolean(getString(R.string.pin_saved), false)
        if (pinSaved) {
            proceedToCalculator()
        } else {
            setContentView(R.layout.activity_splash)
            initUI()
        }
    }

    private fun initUI() {
        val fragment: PinFragment = PinFragment.Companion.getInstance(mPinSetListener)
        supportFragmentManager.beginTransaction().add(R.id.container, fragment).commit()
    }

    private fun proceedToListScreen() {
        val intent = Intent(this@SplashActivity, LockerActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun proceedToCalculator() {
        val intent = Intent(this, CalculatorActivity::class.java)
        startActivity(intent)
        finish()
    }

    private val mPinSetListener = object : IPinSetListener {
        override fun pinSet(code: String) {
            val pin = Pin()
            pin.pin = code
            val realm = RealmManager.defaultInstance(baseContext)
            realm?.beginTransaction()
            realm?.copyToRealm(pin)
            realm?.commitTransaction()
            realm?.where(Pin::class.java)?.findFirst()?.pin?.let { Log.e("pin set", it) }
            val preferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE)
            val editor = preferences.edit()
            editor.putBoolean(getString(R.string.pin_saved), true)
            editor.commit()
            proceedToListScreen()
        }
    }
}