package com.adwait.passlocker.ui.locker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.adwait.passlocker.backend.Models.AddtionalAttribute
import com.adwait.passlocker.backend.Models.BasicUserPasswordInfo
import com.adwait.passlocker.backend.RealmManager
import com.adwait.passlocker.R
import com.adwait.passlocker.ui.customViews.NewRow
import com.adwait.passlocker.ui.customViews.NewRow.RowDeletionListener
import io.realm.RealmList
import java.util.*

/**
 * Created by Adwait on 7/14/2017.
 */
class LoginDetailsFragment : Fragment() {
    private var mNameField: EditText? = null
    private var mHostNameField: EditText? = null
    private var mPassworddField: EditText? = null
    private var mSaveButton: Button? = null
    private var addRowButton: Button? = null
    private var mInfo: BasicUserPasswordInfo? = null
    private var container: LinearLayout? = null
    private val rows = ArrayList<NewRow>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        if (mInfo != null) {
            populateFields()
        }
    }

    override fun onResume() {
        super.onResume()
        if (mInfo != null && mInfo?.name != null) {
            (activity as LockerActivity?)?.setTitle(mInfo?.name)
        } else {
            (activity as LockerActivity?)?.setTitle("Add new Login Details")
        }
    }

    private fun initViews(view: View) {
        mNameField = view.findViewById<View>(R.id.userNameField) as EditText
        mPassworddField = view.findViewById<View>(R.id.passwordField) as EditText
        mHostNameField = view.findViewById<View>(R.id.hostNameField) as EditText
        addRowButton = view.findViewById<View>(R.id.addButton) as Button
        mSaveButton = view.findViewById<View>(R.id.saveButton) as Button
        mSaveButton!!.setOnClickListener { saveInfo() }
        addRowButton = view.findViewById<View>(R.id.addButton) as Button
        container = view.findViewById<View>(R.id.scrollContainer) as LinearLayout
        addRowButton?.setOnClickListener {
            val row = NewRow(context, mRowDeletionListener)
            rows.add(row)
            container?.addView(row, container?.childCount!! - 1)
        }
    }

    private fun populateFields() {
        mNameField?.setText(mInfo?.name.toString())
        mPassworddField?.setText(mInfo?.password)
        mHostNameField?.setText(mInfo?.host.toString())
        (mInfo?.addedAttributes as? RealmList<AddtionalAttribute>)?.let {
            for (attribute in it) {
                var newRow = NewRow(context, mRowDeletionListener)
                newRow.label.setText(attribute.key)
                newRow.value.setText(attribute.value)
                rows.add(newRow)
                container?.addView(newRow, (container?.childCount ?:1) - 1)
            }
        }
    }

    private fun saveInfo() {
        if (mInfo == null) {
            mInfo = BasicUserPasswordInfo()
        }
        RealmManager.defaultInstance(null)?.beginTransaction()
        if (mNameField!!.text != null) mInfo?.name = mNameField?.text.toString()
        if (mHostNameField!!.text != null) mInfo?.host = mHostNameField?.text.toString()
        if (mPassworddField!!.text != null) mInfo?.password = mPassworddField?.text.toString()
        if (rows.size > 0) {
            val additionalAttributes = RealmList<AddtionalAttribute>()
            for (row in rows) {
                if (row.label.text != null) {
                    val attr = RealmManager.defaultInstance(null)!!.createObject(AddtionalAttribute::class.java)
                    attr.value = row.value.text.toString()
                    attr.key = row.label.text.toString()
                    additionalAttributes.add(attr)
                }
            }
            mInfo?.addedAttributes = additionalAttributes
        }
        RealmManager.defaultInstance(null)!!.copyToRealmOrUpdate<BasicUserPasswordInfo>(mInfo)
        RealmManager.defaultInstance(null)!!.commitTransaction()
        requireActivity()?.supportFragmentManager.popBackStackImmediate()
    }

    var mRowDeletionListener = object : RowDeletionListener {
        override fun rowDeleted(row: NewRow?) {
            rows.remove(row)
            container?.removeView(row)
        }
    }

    companion object {
        fun newInstance(info: BasicUserPasswordInfo?): LoginDetailsFragment {
            val fragment = LoginDetailsFragment()
            fragment.mInfo = info
            return fragment
        }
    }
}