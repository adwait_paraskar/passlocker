package com.adwait.passlocker.ui.calculator

/**
 * Created by Adwait on 6/10/2017.
 */
interface INumberPadListener {
    enum class Operator {
        NONE, ADD {
            override fun toString(): String {
                return "+"
            }
        },
        SUBTRACT {
            override fun toString(): String {
                return "-"
            }
        },
        MULTIPLY {
            override fun toString(): String {
                return "*"
            }
        },
        DIVIDE {
            override fun toString(): String {
                return "/"
            }
        },
        EQUALS {
            override fun toString(): String {
                return "="
            }
        },
        DOT {
            override fun toString(): String {
                return "."
            }
        },
        AC {
            override fun toString(): String {
                return "AC"
            }
        },
        DELETE {
            override fun toString(): String {
                return "<-"
            }
        }
    }

    fun numberClicked(number: Int)
    fun operatorClicked(operator: Operator)
}