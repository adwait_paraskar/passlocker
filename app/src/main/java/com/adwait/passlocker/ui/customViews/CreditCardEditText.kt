package com.adwait.passlocker.ui.customViews

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.SparseArray
import com.adwait.passlocker.R
import com.google.android.material.textfield.TextInputEditText
import java.util.regex.Pattern

/**
 * Created by Adwait on 6/17/2017.
 */
class CreditCardEditText : TextInputEditText {
    private lateinit var mCCPatterns: SparseArray<Pattern>
    private var mCurrentDrawable: Drawable? = null
    private var mCurrentDrawableResId = 0
    private val mDefaultDrawableResId = R.drawable.creditcard

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        addTextChangedListener(watcher)
            mCCPatterns = SparseArray()
            // With spaces for credit card masking
            mCCPatterns.put(R.drawable.visa, Pattern.compile(
                    "^4[0-9]{2,12}(?:[0-9]{3})?$"))
            mCCPatterns.put(R.drawable.mastercard, Pattern.compile(
                    "^5[1-5][0-9]{1,14}$"))
            mCCPatterns.put(R.drawable.amex, Pattern.compile(
                    "^3[47][0-9]{1,13}$"))
    }

    val cardNo: String?
        get() = this.text.toString().replace(" ", "")

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (mCurrentDrawable == null) {
            return
        }
        // right offset for showing errors in the EditText
        var rightOffset = 0
        if (error != null && error.length > 0) {
            rightOffset = resources.displayMetrics.density.toInt() * 32
        }
        val right = width - paddingRight - rightOffset
        val top = paddingTop
        val bottom = height - paddingBottom
        val ratio = (mCurrentDrawable?.intrinsicWidth ?: 1.0 ).toFloat() /
                (mCurrentDrawable?.intrinsicHeight?: 1).toFloat()
        //scale image depending on height available.
        val left = (right - (bottom - top) * ratio).toInt()
        mCurrentDrawable?.setBounds(left, top, right, bottom)
        mCurrentDrawable?.draw(canvas)
    }

    private val watcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(text: CharSequence, start: Int, lengthBefore: Int, lengthAfter: Int) {
            var mDrawableResId = 0
            val cardNo = text.toString().replace(" ", "")
            for (i in 0 until mCCPatterns.size()) {
                val key = mCCPatterns.keyAt(i)
                // get the object by the key.
                val p = mCCPatterns[key]
                val m = p.matcher(cardNo)
                if (m.find()) {
                    mDrawableResId = key
                    break
                }
            }
            if (mDrawableResId > 0 && mDrawableResId !=
                    mCurrentDrawableResId) {
                mCurrentDrawableResId = mDrawableResId
            } else if (mDrawableResId == 0) {
                mCurrentDrawableResId = mDefaultDrawableResId
            }
            mCurrentDrawable = resources
                    .getDrawable(mCurrentDrawableResId)
        }

        var space = ' '
        var i = 0
        override fun afterTextChanged(s: Editable) {
            if (s.length > 0) {
                i = 0
                while (i < s.length) {
                    if (s[i] == space) {
                        if (i + 1 == s.length || (i + 1) % 5 != 0) {
                            s.delete(i, i + 1)
                        }
                    }
                    i++
                }
                i = 0
                while (i < s.length) {
                    if (i > 0 && (i + 1) % 5 == 0 && s[i] != space) {
                        s.insert(i, space.toString() + "")
                    }
                    i++
                }
            }
            //4,9,
        }
    }
}