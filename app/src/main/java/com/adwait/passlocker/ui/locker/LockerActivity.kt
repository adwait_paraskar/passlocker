package com.adwait.passlocker.ui.locker

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.adwait.passlocker.R

/**
 * Created by Adwait on 5/27/2017.
 */
class LockerActivity : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    private val listTag = "LIST"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locker)
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        if (savedInstanceState == null) {
            val fragment = ListFragment()
            supportFragmentManager.beginTransaction().add(R.id.container, fragment, listTag).commit()
        }
    }

    fun navigateToFragment(fragment: Fragment?, tag: String?) {
        supportFragmentManager.beginTransaction().replace(R.id.container, fragment!!).addToBackStack(tag).commit()
    }

    fun popToState(tag: String?) {
        supportFragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun popToList() {
        popToState(listTag)
    }

    fun setTitle(title: String?) {
        supportActionBar!!.title = title
    }
}