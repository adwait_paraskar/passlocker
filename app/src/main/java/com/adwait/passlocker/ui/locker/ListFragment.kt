package com.adwait.passlocker.ui.locker

import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.view.View.OnCreateContextMenuListener
import android.view.View.OnLongClickListener
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adwait.passlocker.backend.Models.*
import com.adwait.passlocker.backend.RealmManager
import com.adwait.passlocker.R
import com.adwait.passlocker.ui.customViews.IPinSetListener
import com.adwait.passlocker.ui.customViews.PinFragment
import com.github.clans.fab.FloatingActionButton
import io.realm.RealmObject
import java.util.*

/**
 * Created by Adwait on 6/3/2017.
 */
class ListFragment : Fragment() {
    private var listView: RecyclerView? = null
    private var mAdapter: RecyclerAdapter? = null
    private var fabCreditCard: FloatingActionButton? = null
    private var fabBanking: FloatingActionButton? = null
    private var fabLogin: FloatingActionButton? = null
    private var items: ArrayList<IBasicInfo>? = null
    private var selectedIndex = 0
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        fabCreditCard = view.findViewById<View>(R.id.fabCreditCard) as FloatingActionButton
        fabCreditCard!!.setOnClickListener {
            val fragment: CreditCardEditFragment = CreditCardEditFragment.Companion.newInstance(null)
            (activity as LockerActivity?)!!.navigateToFragment(fragment, "List")
        }
        fabBanking = view.findViewById<View>(R.id.fabBanking) as FloatingActionButton
        fabBanking!!.setOnClickListener {
            val fragment: BankDetailsEditFragment = BankDetailsEditFragment.Companion.newInstance(null)
            (activity as LockerActivity?)!!.navigateToFragment(fragment, "List")
        }
        fabLogin = view.findViewById<View>(R.id.fabLogin) as FloatingActionButton
        fabLogin!!.setOnClickListener {
            val fragment: LoginDetailsFragment = LoginDetailsFragment.Companion.newInstance(null)
            (activity as LockerActivity?)!!.navigateToFragment(fragment, "List")
        }
        listView = view.findViewById<View>(R.id.list_view) as RecyclerView
        listView!!.layoutManager = LinearLayoutManager(activity)
        val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        listView!!.addItemDecoration(decoration)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        items = ArrayList()
        items!!.addAll(RealmManager.defaultInstance(null)!!.where(BasicUserPasswordInfo::class.java).findAll())
        items!!.addAll(RealmManager.defaultInstance(null)!!.where(CreditCardInfo::class.java).findAll())
        items!!.addAll(RealmManager.defaultInstance(null)!!.where(NetBankingInfo::class.java).findAll())
        mAdapter = RecyclerAdapter(items!!)
        listView!!.adapter = mAdapter
        registerForContextMenu(listView!!)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        (activity as LockerActivity?)!!.setTitle("Saved Items")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.change -> {
                val fragment: PinFragment = PinFragment.Companion.getInstance(pinSetListener)
                (activity as LockerActivity?)!!.navigateToFragment(fragment, "PIN")
            }
        }
        return true
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.delete) {
            confirmForDeletion(selectedIndex)
        }
        return super.onContextItemSelected(item)
    }

    private fun confirmForDeletion(position: Int) {
        val builder: AlertDialog.Builder
        builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlertDialog.Builder(requireContext(), android.R.style.Theme_Material_Dialog_Alert)
        } else {
            AlertDialog.Builder(requireContext())
        }
        builder.setTitle("Delete entry?")
                .setMessage("Do you want to delete this entry?")
                .setPositiveButton("No") { dialog, which -> dialog.dismiss() }
                .setNegativeButton("Yes") { dialog, which -> deleteItem(position) }.show()
    }

    private fun deleteItem(position: Int) {
        RealmManager.defaultInstance(null)?.executeTransaction {
            (items!![position] as RealmObject).deleteFromRealm()
            items!!.removeAt(position)
            mAdapter!!.notifyDataSetChanged()
        }
    }

    private inner class RecyclerAdapter(list: List<IBasicInfo>) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
        private val mList: ArrayList<IBasicInfo>
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindView(mList[position])
            holder.itemView.setOnClickListener {
                if (mList[position] is CreditCardInfo) {
                    val fragment: CreditCardEditFragment = CreditCardEditFragment.Companion.newInstance(mList[position] as CreditCardInfo)
                    (activity as LockerActivity?)!!.navigateToFragment(fragment, "CreditCard")
                }
                if (mList[position] is BasicUserPasswordInfo) {
                    val fragment: LoginDetailsFragment = LoginDetailsFragment.Companion.newInstance(mList[position] as BasicUserPasswordInfo)
                    (activity as LockerActivity?)!!.navigateToFragment(fragment, "Login")
                }
                if (mList[position] is NetBankingInfo) {
                    val fragment: BankDetailsEditFragment = BankDetailsEditFragment.Companion.newInstance(mList[position] as NetBankingInfo)
                    (activity as LockerActivity?)!!.navigateToFragment(fragment, "NetBanking")
                }
            }
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), OnCreateContextMenuListener, OnLongClickListener {
            private val title: TextView
            private val subTitle: TextView
            fun bindView(info: IBasicInfo) {
                title.text = info.host
                subTitle.text = info.name
            }

            override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenuInfo?) {
                val inflater = activity?.menuInflater
                inflater?.inflate(R.menu.context_menu_list, menu)
            }

            override fun onLongClick(v: View): Boolean {
                selectedIndex = this.adapterPosition
                return false
            }

            init {
                title = itemView.findViewById<View>(R.id.title) as TextView
                subTitle = itemView.findViewById<View>(R.id.subtitle) as TextView
                itemView.setOnCreateContextMenuListener(this)
                itemView.setOnLongClickListener(this)
            }
        }

        init {
            mList = list as ArrayList<IBasicInfo>
        }
    }

    private val pinSetListener = object : IPinSetListener {
        override fun pinSet(enteredPin: String) {
            val realm = RealmManager.defaultInstance(null)
            realm!!.beginTransaction()
            val pin = realm.where(Pin::class.java).findFirst()
            pin.pin = enteredPin
            realm.copyToRealm(pin)
            realm.commitTransaction()
            activity!!.supportFragmentManager.popBackStackImmediate()
        }
    }
}