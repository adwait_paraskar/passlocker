package com.adwait.passlocker.ui.customViews

/**
 * Created by Adwait on 6/18/2017.
 */
class PickerDataSource {
    var starYear = 1990
    var endYear = 2100
    var startMonth = 1
    var endMonth = 12
    var currentMonth = 0
    var currentYear = 0
}