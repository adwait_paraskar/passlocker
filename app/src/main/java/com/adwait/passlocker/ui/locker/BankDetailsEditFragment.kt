package com.adwait.passlocker.ui.locker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.adwait.passlocker.backend.Models.NetBankingInfo
import com.adwait.passlocker.backend.RealmManager
import com.adwait.passlocker.R
import com.google.android.material.textfield.TextInputEditText

/**
 * Created by Adwait on 7/7/2017.
 */
class BankDetailsEditFragment : Fragment() {
    private var mBankingInfo: NetBankingInfo? = null
    private var mNameField: TextInputEditText? = null
    private var mHostField: TextInputEditText? = null
    private var mAccountField: TextInputEditText? = null
    private var mUserIdField: TextInputEditText? = null
    private var mPasswordField: TextInputEditText? = null
    private var mIPinField: TextInputEditText? = null
    private var mTxPasswordField: TextInputEditText? = null
    private var mIFSCField: TextInputEditText? = null
    private var mSaveButton: Button? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bank_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        if (mBankingInfo != null) {
            populateView()
        }
    }

    private fun initView(view: View) {
        mNameField = view.findViewById<View>(R.id.accNameField) as TextInputEditText
        mHostField = view.findViewById<View>(R.id.hostNameField) as TextInputEditText
        mAccountField = view.findViewById<View>(R.id.accNoField) as TextInputEditText
        mUserIdField = view.findViewById<View>(R.id.userIDField) as TextInputEditText
        mPasswordField = view.findViewById<View>(R.id.passwordField) as TextInputEditText
        mIPinField = view.findViewById<View>(R.id.iPInField) as TextInputEditText
        mTxPasswordField = view.findViewById<View>(R.id.transactionPasswordField) as TextInputEditText
        mIFSCField = view.findViewById<View>(R.id.ifscField) as TextInputEditText
        mSaveButton = view.findViewById<View>(R.id.saveButton) as Button
        mSaveButton!!.setOnClickListener(mSaveClickListener)
    }

    private fun populateView() {
        mNameField?.setText(mBankingInfo?.name ?: "")
        mHostField?.setText(mBankingInfo?.host ?: "")
        mAccountField?.setText(mBankingInfo?.accountNo.toString())
        mIFSCField?.setText(mBankingInfo?.iFSC.toString())
        mIPinField?.setText(mBankingInfo?.getiPin())
        mUserIdField?.setText(mBankingInfo?.username.toString())
        mPasswordField?.setText(mBankingInfo?.password.toString())
        mTxPasswordField?.setText(mBankingInfo?.transactionPassword.toString())
    }

    override fun onResume() {
        super.onResume()
        if (mBankingInfo != null) {
            (activity as LockerActivity?)?.setTitle(mBankingInfo?.name)
        } else {
            (activity as LockerActivity?)?.setTitle("Add New Bank Details")
        }
    }

    private val mSaveClickListener = View.OnClickListener {
        if (mBankingInfo == null) {
            mBankingInfo = NetBankingInfo()
        }
        RealmManager.defaultInstance(null)!!.beginTransaction()
        if (mHostField!!.text != null) mBankingInfo?.host = mHostField?.text.toString()
        if (mNameField!!.text != null) mBankingInfo?.name = mNameField?.text.toString()
        if (mAccountField!!.text != null) mBankingInfo?.accountNo = mAccountField?.text.toString()
        if (mIFSCField!!.text != null) mBankingInfo?.iFSC = mIFSCField?.text.toString()
        if (mIPinField!!.text != null) mBankingInfo?.setiPin(mIPinField?.text.toString())
        if (mTxPasswordField!!.text != null) mBankingInfo?.transactionPassword = mTxPasswordField?.text.toString()
        if (mUserIdField!!.text != null) mBankingInfo?.username = mUserIdField?.text.toString()
        if (mPasswordField!!.text != null) mBankingInfo?.password = mPasswordField?.text.toString()
        RealmManager.defaultInstance(null)?.copyToRealmOrUpdate<NetBankingInfo>(mBankingInfo)
        RealmManager.defaultInstance(null)?.commitTransaction()
        activity?.supportFragmentManager?.popBackStackImmediate()
    }

    companion object {
        fun newInstance(bankingInfo: NetBankingInfo?): BankDetailsEditFragment {
            val fragment = BankDetailsEditFragment()
            fragment.mBankingInfo = bankingInfo
            return fragment
        }
    }
}