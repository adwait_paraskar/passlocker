package com.adwait.passlocker.ui.customViews

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import com.adwait.passlocker.R
import java.lang.ref.WeakReference

/**
 * Created by Adwait on 7/23/2017.
 */
class NewRow(context: Context?, listener: RowDeletionListener?) : LinearLayout(context) {
    interface RowDeletionListener {
        fun rowDeleted(row: NewRow?)
    }

    var label: EditText
    var value: EditText
    val delete: ImageButton
    val mlistener: WeakReference<RowDeletionListener?>?
    fun getLabel(): String? {
        return label.text.toString()
    }

    fun getValue(): String? {
        return value.text.toString()
    }

    fun setLabel(text: String?) {
        label.setText(text)
    }

    fun setValue(text: String?) {
        value.setText(text)
    }

    init {
        mlistener = WeakReference(listener)
        orientation = HORIZONTAL
        gravity = Gravity.CENTER_VERTICAL
        val params = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val dpValue = 15 // margin in dips
        val d = context?.resources?.displayMetrics?.density
        val margin = (dpValue * (d?: 0.0f)).toInt()
        params.setMargins(margin, 0, margin, 0)
        layoutParams = params
        val view = LayoutInflater.from(context).inflate(R.layout.view_new_row, this, true)
        label = view.findViewById<View>(R.id.label) as EditText
        value = view.findViewById<View>(R.id.value) as EditText
        delete = view.findViewById<View>(R.id.delete) as ImageButton
        delete.setOnClickListener { mlistener.get()?.rowDeleted(this@NewRow) }
    }
}