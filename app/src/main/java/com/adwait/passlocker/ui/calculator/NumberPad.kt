package com.adwait.passlocker.ui.calculator

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adwait.passlocker.R
import java.util.*

/**
 * Created by Adwait on 6/8/2017.
 */
class NumberPad : FrameLayout {
    private var recyclerView: RecyclerView? = null
    private lateinit var currentContext: Context
    private var pinMode = false
    fun setNumberPadListener(mNumberPadListener: INumberPadListener?) {
        this.mNumberPadListener = mNumberPadListener
    }

    private var mNumberPadListener: INumberPadListener? = null

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        this.currentContext = context
        val resources = context.obtainStyledAttributes(attrs, R.styleable.NumberPad, 0, 0)
        pinMode = resources.getBoolean(R.styleable.NumberPad_pinMode, false)
        resources.recycle()
        LayoutInflater.from(context).inflate(R.layout.pinpad, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        recyclerView = findViewById<View>(R.id.pin_view) as RecyclerView
        val adapter = NumberPadAdapter()
        val layoutManager = GridLayoutManager(context, 3)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = adapter
    }

    private inner class NumberPadAdapter internal constructor() : RecyclerView.Adapter<NumberPadHolder>() {
        var list = ArrayList<String>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberPadHolder {
            val layout = LayoutInflater.from(parent.context).inflate(R.layout.number_view, null)
            return NumberPadHolder(layout)
        }

        override fun onBindViewHolder(holder: NumberPadHolder, position: Int) {
            holder.configure(list[position])
        }

        override fun getItemCount(): Int {
            return list.size
        }

        init {
            if (!pinMode) {
                list.add("AC")
                list.add("<-")
                list.add("=")
            }
            for (i in 1..9) {
                list.add(i.toString() + "")
            }
            if (!pinMode) {
                list.add("*")
                list.add("0")
                list.add("/")
                list.add("+")
                list.add("-")
                list.add(".")
            } else {
                list.add("")
                list.add("0")
                list.add("")
            }
        }
    }

    internal inner class NumberPadHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val number: Button
        fun configure(text: String?) {
            when (text) {
                "+", "-", "/", "*", ".", "AC", "<-" -> number.setTextColor(resources.getColor(R.color.colorPrimary))
                "=" -> number.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                else -> {
                }
            }
            number.text = text
        }

        var listener = OnClickListener { v ->
            val title = (v as Button).text.toString()
            when (title) {
                "+" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.ADD)
                "-" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.SUBTRACT)
                "/" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.DIVIDE)
                "*" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.MULTIPLY)
                "." -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.DOT)
                "AC" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.AC)
                "<-" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.DELETE)
                "=" -> mNumberPadListener!!.operatorClicked(INumberPadListener.Operator.EQUALS)
                else -> try {
                    val number = title.toInt()
                    mNumberPadListener?.numberClicked(number)
                } catch (exception: NumberFormatException) {
                }
            }
        }

        init {
            number = itemView.findViewById<View>(R.id.number) as Button
            number.setOnClickListener(listener)
        }
    }
}