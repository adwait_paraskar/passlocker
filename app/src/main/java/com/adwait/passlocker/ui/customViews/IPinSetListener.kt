package com.adwait.passlocker.ui.customViews

/**
 * Created by Adwait on 8/15/2017.
 */
interface IPinSetListener {
    fun pinSet(pin: String)
}